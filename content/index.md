# LAVA

**LAVA** stand for **L**inaro **A**utomated **V**alidation **A**rchitecture.

LAVA is a *continuous integration* system for deploying operating systems onto
physical and virtual hardware for running tests.

LAVA is also used to managed and share boards among teams.

## Use cases

LAVA can be used for a large diversity of tests.

Tests can be simple boot testing, bootloader testing and system level testing,
although extra hardware may be required for some system tests.
Results are tracked over time and data can be exported for further analysis.

LAVA is used in large testing systems like [Linux Kernel Functional Testing](https://lkft.linaro.org/) or [KernelCI](https://kernelci.org).

## Source code

The LAVA source code is hosted on [lavasoftware GitLab](https://git.lavasoftware.org)
