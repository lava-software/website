# Selected talks

## Linaro Connects

### Linaro Tech Days 2020

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/y-5DjzQztAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### San Diego 2019
 
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/lAJAK1jOaNM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Bangkok 2019

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mSC9hKYN7gY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ffsTfI_3y-M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Vancouver 2018

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Ou2L03fSow0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LMs7vCGv8as" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Hong-Kong 2018

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/kB-eUKnYy7g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jHyanD1II90" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
