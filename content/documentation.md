# Documentation

## Embeded documentation

Each instance of LAVA comes with the documentation embedded.

You can read the documentation in the 
[demo LAVA instance](https://master.lavasoftware.org/static/docs/v2/index.html).

## Work in progress

We are currently reorganizing and rewritting the documentation from scratch to
better address comments received from the community.

This documentation is available at [read the docs](https://lava.readthedocs.io).
